#!/bin/bash

# Copyright 2016-2022 the authors (see README.md).
#
# This file is part of cloogle-web.
#
# Cloogle-web is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

set -e
set -o pipefail

apt-get update -qq

# PHP dependencies
docker-php-source extract
docker-php-ext-install sockets mysqli
docker-php-source delete

# Logo; birthday patch
apt-get install -qq imagemagick librsvg2-bin
month="$(date +%m)"
day="$(date +%d)"
if [[ "$month" == "02" ]] && [[ "$day" > "15" ]] && [[ "$day" < "24" ]]; then
	patch < birthday.patch
fi
rsvg-convert -w 400 logo.svg > logo.png
convert -resize 200x logo.png logo.png

# Clean installation with indexed libraries, for /src and /doc
apt-get install -qq ca-certificates git jq python3
mkdir -p /opt/clean
# In CI we do not have the TLS certificate
CURL="curl"
[[ $CI ]] && CURL="$CURL -k"
$CURL -Ls https://ftp.cs.ru.nl/Clean/builds/linux-x64/clean-language-report-linux-x64-latest.tgz\
	| tar xz -C /opt/clean --strip-components=1
../util/fetch_libs.sh /opt/clean/lib

# Build common problems index
git clone https://gitlab.com/cloogle/common-problems /tmp/common-problems
cd /tmp/common-problems
./build_index.py
mv common-problems.json /var
cd -

# JavaScript dependencies
mkdir /var/www/clean-highlighter
curl "$(curl https://registry.npmjs.org/clean-highlighter/ | jq -r '.versions[."dist-tags".latest].dist.tarball')"\
	| tar xzv --strip-components=1 --directory=/var/www/clean-highlighter
mkdir /var/www/clean-doc-markup
curl "$(curl https://registry.npmjs.org/clean-doc-markup/ | jq -r '.versions[."dist-tags".latest].dist.tarball')"\
	| tar xzv --strip-components=1 --directory=/var/www/clean-doc-markup

apt-get remove -qq ca-certificates git imagemagick jq librsvg2-bin python3
apt-get autoremove -qq
