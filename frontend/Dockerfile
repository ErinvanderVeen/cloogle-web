# Copyright 2016-2022 the authors (see README.md).
#
# This file is part of cloogle-web.
#
# Cloogle-web is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

# This is only needed for a TLS certificate for ftp.cs.ru.nl; see below
FROM camilstaps/clean:nightly AS certificate-source

FROM php:7.2-apache

# We need this certificate to connect to ftp.cs.ru.nl to get the Clean
# documentation (see build.sh)
COPY --from=certificate-source /etc/ssl/certs/geant.pem /etc/ssl/certs/geant.pem
RUN cat /etc/ssl/certs/geant.pem >> /etc/ssl/certs/ca-certificates.crt

COPY frontend/build.sh /var/www/build.sh
COPY frontend/logo.svg /var/www/logo.svg
COPY frontend/birthday.patch /var/www/birthday.patch
COPY util /var/util
WORKDIR /var/www
RUN ./build.sh
