<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

require_once('./conf.php');

$sql =
	"SELECT
		count(*),
		count(case when `responsecode` IN (" . SQL_SERVER_ERROR . ") then 1 else null end),
		count(case when `responsecode`>1 and `responsecode`<>" . E_NORESULTS . " and `responsecode` NOT IN (" . SQL_SERVER_ERROR . ") then 1 else null end),
		sum(case when `query` LIKE '%::%' then 1 else null end),
		sum(case when `query` LIKE 'type %' then 1 else null end),
		sum(case when `query` LIKE 'class %' OR `query` LIKE 'instance %' then 1 else null end),
		sum(case when `query` LIKE 'using %' then 1 else null end)
	FROM `log`
	INNER JOIN `useragent` ON `log`.`useragent_id` = `useragent`.`id`
	WHERE
		" . SQL_NOT_SILLYUSER . " AND
		`date` BETWEEN timestamp('$startTime') AND timestamp('$endTime')
		AND ";

$oss = [];
$oss_results = [];
$uas = [];
foreach ($user_agents as $k => $ua) {
	$oss[] = "`useragent` LIKE '" . $ua['pattern'] . "'";
	$oss_results[] = empty($ua['url']) ? $k : $k . ':' . $ua['url'];
	$uas[] = $ua['pattern'];
}
$oss[] = "(`useragent` NOT LIKE '" .
		implode("' AND `useragent` NOT LIKE '", $uas) .
		"') OR `useragent` IS NULL";
$oss_results[] = 'Other';

$results = [
	['name' => 'Success', 'data' => [], 'stack' => 'response'],
	['name' => 'Server error', 'data' => [], 'stack' => 'response'],
	['name' => 'User error', 'data' => [], 'stack' => 'response'],
	['name' => 'Name only', 'data' => [], 'stack' => 'search kind'],
	['name' => 'Unification', 'data' => [], 'stack' => 'search kind'],
	['name' => 'Type', 'data' => [], 'stack' => 'search kind'],
	['name' => 'Class', 'data' => [], 'stack' => 'search kind'],
	['name' => 'Using', 'data' => [], 'stack' => 'search kind']
];

foreach ($oss as $os) {
	$stmt = $db->stmt_init();
	if (!$stmt->prepare($sql . $os))
		var_dump($stmt->error);
	$stmt->execute();
	$stmt->bind_result($total, $servererr, $usererr, $unify, $type, $class, $using);
	$stmt->fetch();
	$stmt->close();

	$total = (int) $total;
	$servererr = (int) $servererr;
	$usererr = (int) $usererr;
	$unify = (int) $unify;
	$type = (int) $type;
	$class = (int) $class;
	$using = (int) $using;

	$results[0]['data'][] = $total - $servererr - $usererr;
	$results[1]['data'][] = $servererr;
	$results[2]['data'][] = $usererr;

	$results[3]['data'][] = $total - $unify - $type - $class - $using;
	$results[4]['data'][] = $unify;
	$results[5]['data'][] = $type;
	$results[6]['data'][] = $class;
	$results[7]['data'][] = $using;
}

$response = ['data' => $results, 'oss' => $oss_results];

header('Content-Type: text/javascript');
echo "$callback(" . json_encode($response) . ");";
