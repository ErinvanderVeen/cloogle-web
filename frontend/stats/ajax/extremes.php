<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

require_once('./conf.php');

$sqlavgreq =
	"SELECT AVG(`number`) as avgn FROM
	(SELECT COUNT(*) as number
		FROM `log`
		WHERE
			" . SQL_NOT_SILLYUSER . " AND
			`date` BETWEEN timestamp('$startTime') AND timestamp('$endTime')
		GROUP BY DATE(`date`)) counts";
$sqlmaxreq =
	"SELECT number as maxn, thedate as maxd FROM
	(SELECT DATE(`date`) as thedate, COUNT(*) as number
		FROM `log`
		WHERE
			" . SQL_NOT_SILLYUSER . " AND
			`date` BETWEEN timestamp('$startTime') AND timestamp('$endTime')
		GROUP BY DATE(`date`)
		ORDER BY number DESC, thedate DESC
		LIMIT 1) counts";
$sqlavgvis =
	"SELECT AVG(number) as avgn FROM
	(SELECT DATE(`date`) as thedate, COUNT(DISTINCT ip, useragent_id) as number
		FROM `log`
		WHERE
			" . SQL_NOT_SILLYUSER . " AND
			`date` BETWEEN timestamp('$startTime') AND timestamp('$endTime')
		GROUP BY DATE(`date`)) counts;";
$sqlmaxvis =
	"SELECT number as maxn, thedate as maxd FROM
	(SELECT DATE(`date`) as thedate, COUNT(DISTINCT ip, useragent_id) as number
		FROM `log`
		WHERE
			" . SQL_NOT_SILLYUSER . " AND
			`date` BETWEEN timestamp('$startTime') AND timestamp('$endTime')
		GROUP BY DATE(`date`)
		ORDER BY number DESC, thedate DESC
		LIMIT 1) counts;";

$stmt = $db->stmt_init();
if (!$stmt->prepare($sqlavgreq))
	var_dump($stmt->error);
$stmt->execute();
$stmt->bind_result($avgr);
$stmt->fetch();
$stmt->close();

$stmt = $db->stmt_init();
if (!$stmt->prepare($sqlmaxreq))
	var_dump($stmt->error);
$stmt->execute();
$stmt->bind_result($maxrn, $maxrd);
$stmt->fetch();
$stmt->close();

$stmt = $db->stmt_init();
if (!$stmt->prepare($sqlavgvis))
	var_dump($stmt->error);
$stmt->execute();
$stmt->bind_result($avgv);
$stmt->fetch();
$stmt->close();

$stmt = $db->stmt_init();
if (!$stmt->prepare($sqlmaxvis))
	var_dump($stmt->error);
$stmt->execute();
$stmt->bind_result($maxvn, $maxvd);
$stmt->fetch();
$stmt->close();

$results =
	[ 'requests' => ['avgn' => $avgr, 'maxn' => $maxrn, 'maxd' => $maxrd]
	, 'visitors' => ['avgn' => $avgv, 'maxn' => $maxvn, 'maxd' => $maxvd]
	];

header('Content-Type: text/javascript');
echo "$callback(" . json_encode($results) . ");";
